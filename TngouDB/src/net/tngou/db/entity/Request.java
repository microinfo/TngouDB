package net.tngou.db.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;



/**
 * 
* @ClassName: Request
* @Description: TODO 执行申请
* @author tngou@tngou.net (www.tngou.net)
* @date 2015年5月20日 下午2:48:58
*
 */
public class Request implements Serializable{

	
	/**
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么)
	*/ 
	private static final long serialVersionUID = 1L;
	private String module; //执行类
	private String action; //执行方法
	private String sql; //执行语句

	
	private Map<String, Object> map= new HashMap<String, Object>(); //参数
	
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	public String getSql() {
		return sql;
	}
	public void setSql(String sql) {
		this.sql = sql;
	}
	public Map<String, Object> getMap() {
		return map;
	}
	public void setMap(Map<String, Object> map) {
		this.map = map;
	}
	
	public Object getParameter(String string) {
		// TODO Auto-generated method stub
		return  map.get(string);
	}
	
	public String getParameterString(String string) {
		// TODO Auto-generated method stub
		return  map.get(string).toString();
	}
	public Object setParameter(String name,Object value) {
		// TODO Auto-generated method stub
		return  map.put(name, value);
	}
	
	
	 
}
